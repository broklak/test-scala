package controllers

import java.util.concurrent.TimeoutException 
import javax.inject.Inject

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global

import play.api.Logger
import play.api.i18n.MessagesApi
import play.api.mvc.{ Action, Controller }
import play.api.data.Form
import play.api.data.Forms.{ date, number, ignored, mapping, nonEmptyText }
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json, Json.toJsFieldJsValueWrapper

import play.modules.reactivemongo.{
  MongoController, ReactiveMongoApi, ReactiveMongoComponents
}
import play.modules.reactivemongo.json._, collection.JSONCollection

import reactivemongo.bson.BSONObjectID

import models.{ Brand, JsonFormats, Page }, JsonFormats.brandFormat 
 
class Brands @Inject() (
  val reactiveMongoApi: ReactiveMongoApi,
  val messagesApi: MessagesApi)
    extends Controller with MongoController with ReactiveMongoComponents {

  implicit val timeout = 10.seconds

  /**
   * Describe the brand form (used in both edit and create screens).
   */ 
  val brandForm = Form( 
    mapping(
      // "id" -> ignored(BSONObjectID.generate: BSONObjectID),
      "id" -> nonEmptyText,
      "name" -> nonEmptyText,  
      "createdDate" -> date("yyyy-MM-dd")
    )
    (Brand.apply)(Brand.unapply)
  )

  def collection: JSONCollection = db.collection[JSONCollection]("brand")

  import play.api.data.Form
  import models._
  import models.JsonFormats._

  def index = Action { Home }
  val Home = Redirect(routes.Brands.list())

  def list(page: Int, orderBy: Int, filter: String) = Action.async { implicit request => 
    val futurePage = if (filter.length > 0) {
      collection.find(Json.obj("name" -> filter)).cursor[Brand]().collect[List]()
    } else collection.genericQueryBuilder.cursor[Brand]().collect[List]()

    futurePage.map { results => Ok(Json.toJson(results)) } 
  } 

  def listall = Action.async { implicit request => 
    val futurePage = collection.genericQueryBuilder.cursor[Brand]().collect[List]() 
    futurePage.map { results => Ok(Json.toJson(results)) } 
  } 
 
  def show(id: String) = Action.async { request =>
    val detailData = collection.find(Json.obj("_id" -> id)).cursor[Brand]().collect[List]()
    detailData.map { results => Ok(Json.toJson(results)) }  
  }

  def save = Action.async { implicit request => 
    brandForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      brand => {
        collection.insert(brand)
        .map { result => Ok(Json.toJson(s"Brand has been added")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in brand update process"))
        }
      }) 
  } 

  def update(id: String) = Action.async { implicit request =>
    brandForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      brand => {
        collection.update(Json.obj("_id" -> id), brand.copy(_id = id))
        .map { result => Ok(Json.toJson(s"Brand has been updated")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in brand update process"))  
        }
      })
  }

  def delete(id: String) = Action.async {
    val action = collection.remove(Json.obj("_id" -> id), firstMatchOnly = true)
    action.map( results => Ok( Json.toJson("Brand has been deleted") ) ).recover {
      case t: TimeoutException =>
        Ok(Json.toJson(s"Problem deleting brand")) 
    }
  }

}
