package controllers

import java.util.concurrent.TimeoutException 
import javax.inject.Inject

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global

import play.api.Logger
import play.api.i18n.MessagesApi
import play.api.mvc.{ Action, Controller }
import play.api.data.Form
import play.api.data.Forms.{ date, number, ignored, mapping, nonEmptyText }
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json, Json.toJsFieldJsValueWrapper

import play.modules.reactivemongo.{
  MongoController, ReactiveMongoApi, ReactiveMongoComponents
}
import play.modules.reactivemongo.json._, collection.JSONCollection

import reactivemongo.bson.BSONObjectID

import models.{ CartItem, JsonFormats, Page }, JsonFormats.cartItemsFormat 
 
class Cartitems @Inject() (
  val reactiveMongoApi: ReactiveMongoApi,
  val messagesApi: MessagesApi)
    extends Controller with MongoController with ReactiveMongoComponents {

  implicit val timeout = 10.seconds

  /**
   * Describe the cartItems form (used in both edit and create screens).
   */ 
  val cartItemsForm = Form( 
    mapping(
      // "id" -> ignored(BSONObjectID.generate: BSONObjectID),
      "id" -> nonEmptyText,
      "cart" -> nonEmptyText,
      "variant" -> nonEmptyText,
      "qty" -> number
    )
    (CartItem.apply)(CartItem.unapply)
  )

  def collection: JSONCollection = db.collection[JSONCollection]("cartitem")

  import play.api.data.Form
  import models._
  import models.JsonFormats._

  def index = Action { Ok("Hello") } 

  def list(cartId: String) = Action.async { implicit request => 
    val futurePage = collection.find(Json.obj("cart" -> cartId)).cursor[CartItem]().collect[List]() 
    futurePage.map { results => Ok(Json.toJson(results)) } 
  } 
 
  def show(cart: String, id: String) = Action.async { request =>
    val detailData = collection.find(Json.obj("_id" -> id, "cart" -> cart)).cursor[CartItem]().collect[List]()
    detailData.map { results => Ok(Json.toJson(results)) }  
  }

  def save(cart: String) = Action.async { implicit request => 
    cartItemsForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      cartitems => {
        collection.insert(cartitems)
        .map { result => Ok(Json.toJson(s"cartitems has been added")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in cartItems update process"))
        }
      }) 
  } 

  def update(cart: String, id: String) = Action.async { implicit request =>
    cartItemsForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      cartitems => {
        collection.update(Json.obj("_id" -> id, "cart" -> cart), cartitems.copy(_id = id, cart = cart))
        .map { result => Ok(Json.toJson(s"cartitems has been updated")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in cartItems update process"))  
        }
      })
  }

  def delete(cart: String, id: String) = Action.async {
    val action = collection.remove(Json.obj("_id" -> id, "cart" -> cart), firstMatchOnly = true)
    action.map( results => Ok( Json.toJson("cartitems has been deleted") ) ).recover {
      case t: TimeoutException =>
        Ok(Json.toJson(s"Problem deleting cartItems")) 
    }
  }

}
