package controllers

import java.util.concurrent.TimeoutException 
import javax.inject.Inject

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global

import play.api.Logger
import play.api.i18n.MessagesApi
import play.api.mvc.{ Action, Controller }
import play.api.data.Form
import play.api.data.Forms.{ date, number, ignored, mapping, nonEmptyText }
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json, Json.toJsFieldJsValueWrapper

import play.modules.reactivemongo.{
  MongoController, ReactiveMongoApi, ReactiveMongoComponents
}
import play.modules.reactivemongo.json._, collection.JSONCollection

import reactivemongo.bson.BSONObjectID

import models.{ Cart, JsonFormats, Page }, JsonFormats.cartFormat 
 
class Carts @Inject() (
  val reactiveMongoApi: ReactiveMongoApi,
  val messagesApi: MessagesApi)
    extends Controller with MongoController with ReactiveMongoComponents {

  implicit val timeout = 10.seconds

  /**
   * Describe the cart form (used in both edit and create screens).
   */ 
  val cartForm = Form( 
    mapping(
      // "id" -> ignored(BSONObjectID.generate: BSONObjectID),
      "id" -> nonEmptyText,
      "code" -> nonEmptyText,
      "voucher" -> nonEmptyText,   
      "totalAmount" -> number,  
      "createdDate" -> date("yyyy-MM-dd")
    )
    (Cart.apply)(Cart.unapply)
  )

  def collection: JSONCollection = db.collection[JSONCollection]("cart")

  import play.api.data.Form
  import models._
  import models.JsonFormats._

  def index = Action { Ok("Hello") } 

  def list = Action.async { implicit request => 
    val futurePage = collection.genericQueryBuilder.cursor[Cart]().collect[List]() 
    futurePage.map { results => Ok(Json.toJson(results)) } 
  } 
 
  def show(id: String) = Action.async { request =>
    val detailData = collection.find(Json.obj("_id" -> id)).cursor[Cart]().collect[List]()
    detailData.map { results => Ok(Json.toJson(results)) }  
  }

  def save = Action.async { implicit request => 
    cartForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      cart => {
        collection.insert(cart)
        .map { result => Ok(Json.toJson(s"Cart has been added")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in cart update process"))
        }
      }) 
  } 

  def update(id: String) = Action.async { implicit request =>
    cartForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      cart => {
        collection.update(Json.obj("_id" -> id), cart.copy(_id = id))
        .map { result => Ok(Json.toJson(s"Cart has been updated")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in cart update process"))  
        }
      })
  }

  def delete(id: String) = Action.async {
    val action = collection.remove(Json.obj("_id" -> id), firstMatchOnly = true)
    action.map( results => Ok( Json.toJson("Cart has been deleted") ) ).recover {
      case t: TimeoutException =>
        Ok(Json.toJson(s"Problem deleting cart")) 
    }
  }

}
