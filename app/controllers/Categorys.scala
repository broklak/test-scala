package controllers

import java.util.concurrent.TimeoutException 
import javax.inject.Inject

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global

import play.api.Logger
import play.api.i18n.MessagesApi
import play.api.mvc.{ Action, Controller }
import play.api.data.Form
import play.api.data.Forms.{ date, number, ignored, mapping, nonEmptyText }
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json, Json.toJsFieldJsValueWrapper

import play.modules.reactivemongo.{
  MongoController, ReactiveMongoApi, ReactiveMongoComponents
}
import play.modules.reactivemongo.json._, collection.JSONCollection

import reactivemongo.bson.BSONObjectID

import models.{ Category, JsonFormats, Page }, JsonFormats.categoryFormat 
 
class Categorys @Inject() (
  val reactiveMongoApi: ReactiveMongoApi,
  val messagesApi: MessagesApi)
    extends Controller with MongoController with ReactiveMongoComponents {

  implicit val timeout = 10.seconds

  /**
   * Describe the category form (used in both edit and create screens).
   */ 
  val categoryForm = Form( 
    mapping(
      "id" -> nonEmptyText,
      // "id" -> ignored(BSONObjectID.generate: BSONObjectID),
      "name" -> nonEmptyText,  
      "createdDate" -> date("yyyy-MM-dd")
    )
    (Category.apply)(Category.unapply)
  )

  def collection: JSONCollection = db.collection[JSONCollection]("category")

  import play.api.data.Form
  import models._
  import models.JsonFormats._

  def index = Action { Home }
  val Home = Redirect(routes.Categorys.list())

  def list(page: Int, orderBy: Int, filter: String) = Action.async { implicit request => 
    val futurePage = if (filter.length > 0) {
      collection.find(Json.obj("name" -> filter)).cursor[Category]().collect[List]()
    } else collection.genericQueryBuilder.cursor[Category]().collect[List]()

    futurePage.map { results => Ok(Json.toJson(results)) } 
  } 

  def listall = Action.async { implicit request => 
    val futurePage = collection.genericQueryBuilder.cursor[Category]().collect[List]() 
    futurePage.map { results => Ok(Json.toJson(results)) } 
  } 
 
  def show(id: String) = Action.async { request =>
    val detailData = collection.find(Json.obj("_id" -> id)).cursor[Category]().collect[List]()
    detailData.map { results => Ok(Json.toJson(results)) }  
  }

  def save = Action.async { implicit request => 
    categoryForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      category => {
        collection.insert(category)
        .map { result => Ok(Json.toJson(s"Category has been added")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in category update process"))
        }
      }) 
  } 

  def update(id: String) = Action.async { implicit request =>
    categoryForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      category => {
        collection.update(Json.obj("_id" -> id), category.copy(_id = id))
        .map { result => Ok(Json.toJson(s"Category has been updated")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in category update process"))  
        }
      })
  }

  def delete(id: String) = Action.async {
    val action = collection.remove(Json.obj("_id" -> id), firstMatchOnly = true)
    action.map( results => Ok( Json.toJson("Category has been deleted") ) ).recover {
      case t: TimeoutException =>
        Ok(Json.toJson(s"Problem deleting category")) 
    }
  }

}
