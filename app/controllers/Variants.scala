package controllers

import java.util.concurrent.TimeoutException 
import javax.inject.Inject

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global

import play.api.Logger
import play.api.i18n.MessagesApi
import play.api.mvc.{ Action, Controller }
import play.api.data.Form
import play.api.data.Forms.{ date, number, ignored, mapping, nonEmptyText }
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json, Json.toJsFieldJsValueWrapper

import play.modules.reactivemongo.{
  MongoController, ReactiveMongoApi, ReactiveMongoComponents
}
import play.modules.reactivemongo.json._, collection.JSONCollection

import reactivemongo.bson.BSONObjectID

import models.{ Variant, JsonFormats, Page }, JsonFormats.variantFormat 
 
class Variants @Inject() (
  val reactiveMongoApi: ReactiveMongoApi,
  val messagesApi: MessagesApi)
    extends Controller with MongoController with ReactiveMongoComponents {

  implicit val timeout = 10.seconds

  /**
   * Describe the variant form (used in both edit and create screens).
   */ 
  val variantForm = Form( 
    mapping(
      // "id"      -> ignored(BSONObjectID.generate: BSONObjectID),
      "id"      -> nonEmptyText,
      "product" -> nonEmptyText, 
      "sku"     -> nonEmptyText,
      "size"    -> nonEmptyText,
      "qty"     -> number,
      "createdDate" -> date("yyyy-MM-dd")
    )
    (Variant.apply)(Variant.unapply)
  )

  def collection: JSONCollection = db.collection[JSONCollection]("variant")

  import play.api.data.Form
  import models._
  import models.JsonFormats._

  def index = Action { Home }
  val Home = Redirect(routes.Variants.list())

  def list(page: Int, orderBy: Int, filter: String) = Action.async { implicit request => 
    val futurePage = if (filter.length > 0) {
      collection.find(Json.obj("name" -> filter)).cursor[Variant]().collect[List]()
    } else collection.genericQueryBuilder.cursor[Variant]().collect[List]()

    futurePage.map { results => Ok(Json.toJson(results)) } 
  } 

  def listall = Action.async { implicit request => 
    val futurePage = collection.genericQueryBuilder.cursor[Variant]().collect[List]() 
    futurePage.map { results => Ok(Json.toJson(results)) } 
  } 
 
  def show(id: String) = Action.async { request =>
    val detailData = collection.find(Json.obj("_id" -> id)).cursor[Variant]().collect[List]()
    detailData.map { results => Ok(Json.toJson(results)) }  
  }

  def save = Action.async { implicit request => 
    variantForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      variant => {
        collection.insert(variant)
        .map { result => Ok(Json.toJson(s"Variant has been added")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in variant update process"))
        }
      }) 
  } 

  def update(id: String) = Action.async { implicit request =>
    variantForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      variant => {
        collection.update(Json.obj("_id" -> id), variant.copy(_id = id))
        .map { result => Ok(Json.toJson(s"Variant has been updated")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in variant update process"))  
        }
      })
  }

  def delete(id: String) = Action.async {
    val action = collection.remove(Json.obj("_id" -> id), firstMatchOnly = true)
    action.map( results => Ok( Json.toJson("Variant has been deleted") ) ).recover {
      case t: TimeoutException =>
        Ok(Json.toJson(s"Problem deleting variant")) 
    }
  }

}
