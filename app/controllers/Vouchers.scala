package controllers

import java.util.concurrent.TimeoutException 
import javax.inject.Inject

import scala.concurrent.Future
import scala.concurrent.duration.DurationInt
import scala.concurrent.ExecutionContext.Implicits.global

import play.api.Logger
import play.api.i18n.MessagesApi
import play.api.mvc.{ Action, Controller }
import play.api.data.Form
import play.api.data.Forms.{ date, number, ignored, mapping, nonEmptyText }
import play.api.libs.concurrent.Execution.Implicits.defaultContext
import play.api.libs.json.Json, Json.toJsFieldJsValueWrapper

import play.modules.reactivemongo.{
  MongoController, ReactiveMongoApi, ReactiveMongoComponents
}
import play.modules.reactivemongo.json._, collection.JSONCollection

import reactivemongo.bson.BSONObjectID

import models.{ Voucher, JsonFormats, Page }, JsonFormats.voucherFormat 
 
class Vouchers @Inject() (
  val reactiveMongoApi: ReactiveMongoApi,
  val messagesApi: MessagesApi)
    extends Controller with MongoController with ReactiveMongoComponents {

  implicit val timeout = 10.seconds

  /**
   * Describe the voucher form (used in both edit and create screens).
   */ 
  val voucherForm = Form( 
    mapping(
      // "id" -> ignored(BSONObjectID.generate: BSONObjectID),
      "id" -> nonEmptyText,
      "name" -> nonEmptyText,  
      "code" -> nonEmptyText,  
      "amount" -> number,  
      "createdDate" -> date("yyyy-MM-dd")
    )
    (Voucher.apply)(Voucher.unapply)
  )

  def collection: JSONCollection = db.collection[JSONCollection]("voucher")

  import play.api.data.Form
  import models._
  import models.JsonFormats._

  def index = Action { Home }
  val Home = Redirect(routes.Vouchers.list())

  def list(page: Int, orderBy: Int, filter: String) = Action.async { implicit request => 
    val futurePage = if (filter.length > 0) {
      collection.find(Json.obj("name" -> filter)).cursor[Voucher]().collect[List]()
    } else collection.genericQueryBuilder.cursor[Voucher]().collect[List]()

    futurePage.map { results => Ok(Json.toJson(results)) } 
  } 

  def listall = Action.async { implicit request => 
    val futurePage = collection.genericQueryBuilder.cursor[Voucher]().collect[List]() 
    futurePage.map { results => Ok(Json.toJson(results)) } 
  } 
 
  def show(id: String) = Action.async { request =>
    val detailData = collection.find(Json.obj("_id" -> id)).cursor[Voucher]().collect[List]()
    detailData.map { results => Ok(Json.toJson(results)) }  
  }

  def save = Action.async { implicit request => 
    voucherForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      voucher => {
        collection.insert(voucher)
        .map { result => Ok(Json.toJson(s"Voucher has been added")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in voucher update process"))
        }
      }) 
  } 

  def update(id: String) = Action.async { implicit request =>
    voucherForm.bindFromRequest.fold(
      { formWithErrors =>
        implicit val msg = messagesApi.preferred(request)
        Future.successful(BadRequest(s"Form still wrong")) 
      },
      voucher => {
        collection.update(Json.obj("_id" -> id), voucher.copy(_id = id))
        .map { result => Ok(Json.toJson(s"Voucher has been updated")) }
        .recover {
          case t: TimeoutException =>
            Ok(Json.toJson(s"Problem found in voucher update process"))  
        }
      })
  }

  def delete(id: String) = Action.async {
    val action = collection.remove(Json.obj("_id" -> id), firstMatchOnly = true)
    action.map( results => Ok( Json.toJson("Voucher has been deleted") ) ).recover {
      case t: TimeoutException =>
        Ok(Json.toJson(s"Problem deleting voucher")) 
    }
  }

}
