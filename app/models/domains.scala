package models

import reactivemongo.bson.BSONObjectID
import java.util.Date

case class Product(_id: String, name: String, category: String, brand: String, price: Int, createdDate: Date) 
case class Category(_id: String, name: String, createdDate: Date) 
case class Brand(_id: String, name: String, createdDate: Date) 
case class Variant(_id: String, product: String, sku: String, size: String, qty: Int, createdDate: Date) 
case class Voucher(_id: String, name: String, code: String, amount: Int, createdDate: Date) 

//Cart Temporary
case class Cart(_id: String, code: String, voucher: String, totalAmount: Int, createdDate: Date) 
case class CartItem(_id: String, cart: String, variant: String, qty: Int) 
 
case class Page[A](items: Seq[A], page: Int, offset: Long, total: Long) {
  lazy val prev = Option(page - 1).filter(_ >= 0)
  lazy val next = Option(page + 1).filter(_ => (offset + items.size) < total)
}

object JsonFormats {
  import play.api.libs.json.Json
  import play.api.data._
  import play.api.data.Forms._
  import play.modules.reactivemongo.json.BSONFormats._
 
  implicit val productFormat 	= Json.format[Product] 
  implicit val categoryFormat = Json.format[Category] 
  implicit val brandFormat    = Json.format[Brand] 
  implicit val variantFormat  = Json.format[Variant] 
  implicit val voucherFormat  = Json.format[Voucher] 
  implicit val cartFormat     = Json.format[Cart] 
  implicit val cartItemsFormat= Json.format[CartItem] 
}
