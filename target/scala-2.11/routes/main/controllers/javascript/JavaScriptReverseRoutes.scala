
// @GENERATOR:play-routes-compiler
// @SOURCE:/Volumes/Data/hrm/conf/routes
// @DATE:Thu Dec 31 23:11:17 WIB 2015

import play.api.routing.JavaScriptReverseRoute
import play.api.mvc.{ QueryStringBindable, PathBindable, Call, JavascriptLiteral }
import play.core.routing.{ HandlerDef, ReverseRouteContext, queryString, dynamicString }


import _root_.controllers.Assets.Asset

// @LINE:6
package controllers.javascript {
  import ReverseRouteContext.empty

  // @LINE:41
  class ReverseVariants(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:44
    def show: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Variants.show",
      """
        function(id) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/variants/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
    // @LINE:46
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Variants.delete",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/variants/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/delete"})
        }
      """
    )
  
    // @LINE:41
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Variants.list",
      """
        function(p,s,f) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/variants" + _qS([(p == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("p", p)), (s == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("s", s)), (f == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("f", f))])})
        }
      """
    )
  
    // @LINE:43
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Variants.save",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/variants/save"})
        }
      """
    )
  
    // @LINE:42
    def listall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Variants.listall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/variants/all"})
        }
      """
    )
  
    // @LINE:45
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Variants.update",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/variants/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
  }

  // @LINE:49
  class ReverseCarts(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:51
    def show: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Carts.show",
      """
        function(id) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/carts/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
    // @LINE:49
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Carts.list",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/carts"})
        }
      """
    )
  
    // @LINE:53
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Carts.delete",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/carts/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/delete"})
        }
      """
    )
  
    // @LINE:50
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Carts.save",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/carts/save"})
        }
      """
    )
  
    // @LINE:52
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Carts.update",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/carts/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
  }

  // @LINE:57
  class ReverseCartitems(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:57
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Cartitems.list",
      """
        function(cart) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cartitems/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("cart", encodeURIComponent(cart))})
        }
      """
    )
  
    // @LINE:60
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Cartitems.update",
      """
        function(cart,id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cartitems/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("cart", encodeURIComponent(cart)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
    // @LINE:58
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Cartitems.save",
      """
        function(cart) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cartitems/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("cart", encodeURIComponent(cart)) + "/save"})
        }
      """
    )
  
    // @LINE:61
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Cartitems.delete",
      """
        function(cart,id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cartitems/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("cart", encodeURIComponent(cart)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/delete"})
        }
      """
    )
  
    // @LINE:59
    def show: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Cartitems.show",
      """
        function(cart,id) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/cartitems/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("cart", encodeURIComponent(cart)) + "/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
  }

  // @LINE:17
  class ReverseBrands(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:20
    def show: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Brands.show",
      """
        function(id) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/brands/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
    // @LINE:22
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Brands.delete",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/brands/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/delete"})
        }
      """
    )
  
    // @LINE:17
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Brands.list",
      """
        function(p,s,f) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/brands" + _qS([(p == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("p", p)), (s == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("s", s)), (f == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("f", f))])})
        }
      """
    )
  
    // @LINE:19
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Brands.save",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/brands/save"})
        }
      """
    )
  
    // @LINE:18
    def listall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Brands.listall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/brands/all"})
        }
      """
    )
  
    // @LINE:21
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Brands.update",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/brands/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
  }

  // @LINE:33
  class ReverseVouchers(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:36
    def show: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Vouchers.show",
      """
        function(id) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vouchers/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
    // @LINE:38
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Vouchers.delete",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vouchers/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/delete"})
        }
      """
    )
  
    // @LINE:33
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Vouchers.list",
      """
        function(p,s,f) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vouchers" + _qS([(p == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("p", p)), (s == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("s", s)), (f == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("f", f))])})
        }
      """
    )
  
    // @LINE:35
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Vouchers.save",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vouchers/save"})
        }
      """
    )
  
    // @LINE:34
    def listall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Vouchers.listall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vouchers/all"})
        }
      """
    )
  
    // @LINE:37
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Vouchers.update",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/vouchers/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
  }

  // @LINE:25
  class ReverseCategorys(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:28
    def show: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Categorys.show",
      """
        function(id) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/categorys/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
    // @LINE:30
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Categorys.delete",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/categorys/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/delete"})
        }
      """
    )
  
    // @LINE:25
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Categorys.list",
      """
        function(p,s,f) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/categorys" + _qS([(p == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("p", p)), (s == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("s", s)), (f == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("f", f))])})
        }
      """
    )
  
    // @LINE:27
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Categorys.save",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/categorys/save"})
        }
      """
    )
  
    // @LINE:26
    def listall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Categorys.listall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/categorys/all"})
        }
      """
    )
  
    // @LINE:29
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Categorys.update",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/categorys/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
  }

  // @LINE:6
  class ReverseProducts(_prefix: => String) {

    def _defaultPrefix: String = {
      if (_prefix.endsWith("/")) "" else "/"
    }

  
    // @LINE:12
    def show: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Products.show",
      """
        function(id) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
    // @LINE:14
    def delete: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Products.delete",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id)) + "/delete"})
        }
      """
    )
  
    // @LINE:9
    def list: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Products.list",
      """
        function(p,s,f) {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products" + _qS([(p == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("p", p)), (s == null ? null : (""" + implicitly[QueryStringBindable[Int]].javascriptUnbind + """)("s", s)), (f == null ? null : (""" + implicitly[QueryStringBindable[String]].javascriptUnbind + """)("f", f))])})
        }
      """
    )
  
    // @LINE:11
    def save: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Products.save",
      """
        function() {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products/save"})
        }
      """
    )
  
    // @LINE:10
    def listall: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Products.listall",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products/all"})
        }
      """
    )
  
    // @LINE:13
    def update: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Products.update",
      """
        function(id) {
          return _wA({method:"POST", url:"""" + _prefix + { _defaultPrefix } + """" + "api/products/" + (""" + implicitly[PathBindable[String]].javascriptUnbind + """)("id", encodeURIComponent(id))})
        }
      """
    )
  
    // @LINE:6
    def index: JavaScriptReverseRoute = JavaScriptReverseRoute(
      "controllers.Products.index",
      """
        function() {
          return _wA({method:"GET", url:"""" + _prefix + """"})
        }
      """
    )
  
  }


}