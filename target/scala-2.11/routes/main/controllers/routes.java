
// @GENERATOR:play-routes-compiler
// @SOURCE:/Volumes/Data/hrm/conf/routes
// @DATE:Thu Dec 31 23:11:17 WIB 2015

package controllers;

import router.RoutesPrefix;

public class routes {
  
  public static final controllers.ReverseVariants Variants = new controllers.ReverseVariants(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCarts Carts = new controllers.ReverseCarts(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCartitems Cartitems = new controllers.ReverseCartitems(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseBrands Brands = new controllers.ReverseBrands(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseVouchers Vouchers = new controllers.ReverseVouchers(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseCategorys Categorys = new controllers.ReverseCategorys(RoutesPrefix.byNamePrefix());
  public static final controllers.ReverseProducts Products = new controllers.ReverseProducts(RoutesPrefix.byNamePrefix());

  public static class javascript {
    
    public static final controllers.javascript.ReverseVariants Variants = new controllers.javascript.ReverseVariants(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCarts Carts = new controllers.javascript.ReverseCarts(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCartitems Cartitems = new controllers.javascript.ReverseCartitems(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseBrands Brands = new controllers.javascript.ReverseBrands(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseVouchers Vouchers = new controllers.javascript.ReverseVouchers(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseCategorys Categorys = new controllers.javascript.ReverseCategorys(RoutesPrefix.byNamePrefix());
    public static final controllers.javascript.ReverseProducts Products = new controllers.javascript.ReverseProducts(RoutesPrefix.byNamePrefix());
  }

}
