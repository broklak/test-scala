
// @GENERATOR:play-routes-compiler
// @SOURCE:/Volumes/Data/hrm/conf/routes
// @DATE:Thu Dec 31 23:11:17 WIB 2015

package router

import play.core.routing._
import play.core.routing.HandlerInvokerFactory._
import play.core.j._

import play.api.mvc._

import _root_.controllers.Assets.Asset

class Routes(
  override val errorHandler: play.api.http.HttpErrorHandler, 
  // @LINE:6
  Products_2: controllers.Products,
  // @LINE:17
  Brands_1: controllers.Brands,
  // @LINE:25
  Categorys_0: controllers.Categorys,
  // @LINE:33
  Vouchers_5: controllers.Vouchers,
  // @LINE:41
  Variants_3: controllers.Variants,
  // @LINE:49
  Carts_6: controllers.Carts,
  // @LINE:57
  Cartitems_4: controllers.Cartitems,
  val prefix: String
) extends GeneratedRouter {

   @javax.inject.Inject()
   def this(errorHandler: play.api.http.HttpErrorHandler,
    // @LINE:6
    Products_2: controllers.Products,
    // @LINE:17
    Brands_1: controllers.Brands,
    // @LINE:25
    Categorys_0: controllers.Categorys,
    // @LINE:33
    Vouchers_5: controllers.Vouchers,
    // @LINE:41
    Variants_3: controllers.Variants,
    // @LINE:49
    Carts_6: controllers.Carts,
    // @LINE:57
    Cartitems_4: controllers.Cartitems
  ) = this(errorHandler, Products_2, Brands_1, Categorys_0, Vouchers_5, Variants_3, Carts_6, Cartitems_4, "/")

  import ReverseRouteContext.empty

  def withPrefix(prefix: String): Routes = {
    router.RoutesPrefix.setPrefix(prefix)
    new Routes(errorHandler, Products_2, Brands_1, Categorys_0, Vouchers_5, Variants_3, Carts_6, Cartitems_4, prefix)
  }

  private[this] val defaultPrefix: String = {
    if (this.prefix.endsWith("/")) "" else "/"
  }

  def documentation = List(
    ("""GET""", this.prefix, """controllers.Products.index"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products""", """controllers.Products.list(p:Int ?= 0, s:Int ?= 2, f:String ?= "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products/all""", """controllers.Products.listall"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products/save""", """controllers.Products.save"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products/$id<[^/]+>""", """controllers.Products.show(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products/$id<[^/]+>""", """controllers.Products.update(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/products/$id<[^/]+>/delete""", """controllers.Products.delete(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/brands""", """controllers.Brands.list(p:Int ?= 0, s:Int ?= 2, f:String ?= "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/brands/all""", """controllers.Brands.listall"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/brands/save""", """controllers.Brands.save"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/brands/$id<[^/]+>""", """controllers.Brands.show(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/brands/$id<[^/]+>""", """controllers.Brands.update(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/brands/$id<[^/]+>/delete""", """controllers.Brands.delete(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/categorys""", """controllers.Categorys.list(p:Int ?= 0, s:Int ?= 2, f:String ?= "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/categorys/all""", """controllers.Categorys.listall"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/categorys/save""", """controllers.Categorys.save"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/categorys/$id<[^/]+>""", """controllers.Categorys.show(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/categorys/$id<[^/]+>""", """controllers.Categorys.update(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/categorys/$id<[^/]+>/delete""", """controllers.Categorys.delete(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/vouchers""", """controllers.Vouchers.list(p:Int ?= 0, s:Int ?= 2, f:String ?= "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/vouchers/all""", """controllers.Vouchers.listall"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/vouchers/save""", """controllers.Vouchers.save"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/vouchers/$id<[^/]+>""", """controllers.Vouchers.show(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/vouchers/$id<[^/]+>""", """controllers.Vouchers.update(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/vouchers/$id<[^/]+>/delete""", """controllers.Vouchers.delete(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/variants""", """controllers.Variants.list(p:Int ?= 0, s:Int ?= 2, f:String ?= "")"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/variants/all""", """controllers.Variants.listall"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/variants/save""", """controllers.Variants.save"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/variants/$id<[^/]+>""", """controllers.Variants.show(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/variants/$id<[^/]+>""", """controllers.Variants.update(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/variants/$id<[^/]+>/delete""", """controllers.Variants.delete(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/carts""", """controllers.Carts.list"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/carts/save""", """controllers.Carts.save"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/carts/$id<[^/]+>""", """controllers.Carts.show(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/carts/$id<[^/]+>""", """controllers.Carts.update(id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/carts/$id<[^/]+>/delete""", """controllers.Carts.delete(id:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cartitems/$cart<[^/]+>""", """controllers.Cartitems.list(cart:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cartitems/$cart<[^/]+>/save""", """controllers.Cartitems.save(cart:String)"""),
    ("""GET""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cartitems/$cart<[^/]+>/$id<[^/]+>""", """controllers.Cartitems.show(cart:String, id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cartitems/$cart<[^/]+>/$id<[^/]+>""", """controllers.Cartitems.update(cart:String, id:String)"""),
    ("""POST""", this.prefix + (if(this.prefix.endsWith("/")) "" else "/") + """api/cartitems/$cart<[^/]+>/$id<[^/]+>/delete""", """controllers.Cartitems.delete(cart:String, id:String)"""),
    Nil
  ).foldLeft(List.empty[(String,String,String)]) { (s,e) => e.asInstanceOf[Any] match {
    case r @ (_,_,_) => s :+ r.asInstanceOf[(String,String,String)]
    case l => s ++ l.asInstanceOf[List[(String,String,String)]]
  }}


  // @LINE:6
  private[this] lazy val controllers_Products_index0_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix)))
  )
  private[this] lazy val controllers_Products_index0_invoker = createInvoker(
    Products_2.index,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Products",
      "index",
      Nil,
      "GET",
      """ Default path will just redirect to the employee list""",
      this.prefix + """"""
    )
  )

  // @LINE:9
  private[this] lazy val controllers_Products_list1_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products")))
  )
  private[this] lazy val controllers_Products_list1_invoker = createInvoker(
    Products_2.list(fakeValue[Int], fakeValue[Int], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Products",
      "list",
      Seq(classOf[Int], classOf[Int], classOf[String]),
      "GET",
      """ Product list (look at the default values for pagination parameters)""",
      this.prefix + """api/products"""
    )
  )

  // @LINE:10
  private[this] lazy val controllers_Products_listall2_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products/all")))
  )
  private[this] lazy val controllers_Products_listall2_invoker = createInvoker(
    Products_2.listall,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Products",
      "listall",
      Nil,
      "GET",
      """""",
      this.prefix + """api/products/all"""
    )
  )

  // @LINE:11
  private[this] lazy val controllers_Products_save3_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products/save")))
  )
  private[this] lazy val controllers_Products_save3_invoker = createInvoker(
    Products_2.save,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Products",
      "save",
      Nil,
      "POST",
      """""",
      this.prefix + """api/products/save"""
    )
  )

  // @LINE:12
  private[this] lazy val controllers_Products_show4_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Products_show4_invoker = createInvoker(
    Products_2.show(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Products",
      "show",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """api/products/$id<[^/]+>"""
    )
  )

  // @LINE:13
  private[this] lazy val controllers_Products_update5_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Products_update5_invoker = createInvoker(
    Products_2.update(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Products",
      "update",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/products/$id<[^/]+>"""
    )
  )

  // @LINE:14
  private[this] lazy val controllers_Products_delete6_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/products/"), DynamicPart("id", """[^/]+""",true), StaticPart("/delete")))
  )
  private[this] lazy val controllers_Products_delete6_invoker = createInvoker(
    Products_2.delete(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Products",
      "delete",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/products/$id<[^/]+>/delete"""
    )
  )

  // @LINE:17
  private[this] lazy val controllers_Brands_list7_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/brands")))
  )
  private[this] lazy val controllers_Brands_list7_invoker = createInvoker(
    Brands_1.list(fakeValue[Int], fakeValue[Int], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Brands",
      "list",
      Seq(classOf[Int], classOf[Int], classOf[String]),
      "GET",
      """ Brands list (look at the default values for pagination parameters)""",
      this.prefix + """api/brands"""
    )
  )

  // @LINE:18
  private[this] lazy val controllers_Brands_listall8_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/brands/all")))
  )
  private[this] lazy val controllers_Brands_listall8_invoker = createInvoker(
    Brands_1.listall,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Brands",
      "listall",
      Nil,
      "GET",
      """""",
      this.prefix + """api/brands/all"""
    )
  )

  // @LINE:19
  private[this] lazy val controllers_Brands_save9_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/brands/save")))
  )
  private[this] lazy val controllers_Brands_save9_invoker = createInvoker(
    Brands_1.save,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Brands",
      "save",
      Nil,
      "POST",
      """""",
      this.prefix + """api/brands/save"""
    )
  )

  // @LINE:20
  private[this] lazy val controllers_Brands_show10_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/brands/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Brands_show10_invoker = createInvoker(
    Brands_1.show(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Brands",
      "show",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """api/brands/$id<[^/]+>"""
    )
  )

  // @LINE:21
  private[this] lazy val controllers_Brands_update11_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/brands/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Brands_update11_invoker = createInvoker(
    Brands_1.update(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Brands",
      "update",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/brands/$id<[^/]+>"""
    )
  )

  // @LINE:22
  private[this] lazy val controllers_Brands_delete12_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/brands/"), DynamicPart("id", """[^/]+""",true), StaticPart("/delete")))
  )
  private[this] lazy val controllers_Brands_delete12_invoker = createInvoker(
    Brands_1.delete(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Brands",
      "delete",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/brands/$id<[^/]+>/delete"""
    )
  )

  // @LINE:25
  private[this] lazy val controllers_Categorys_list13_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/categorys")))
  )
  private[this] lazy val controllers_Categorys_list13_invoker = createInvoker(
    Categorys_0.list(fakeValue[Int], fakeValue[Int], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Categorys",
      "list",
      Seq(classOf[Int], classOf[Int], classOf[String]),
      "GET",
      """ Categorys list (look at the default values for pagination parameters)""",
      this.prefix + """api/categorys"""
    )
  )

  // @LINE:26
  private[this] lazy val controllers_Categorys_listall14_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/categorys/all")))
  )
  private[this] lazy val controllers_Categorys_listall14_invoker = createInvoker(
    Categorys_0.listall,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Categorys",
      "listall",
      Nil,
      "GET",
      """""",
      this.prefix + """api/categorys/all"""
    )
  )

  // @LINE:27
  private[this] lazy val controllers_Categorys_save15_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/categorys/save")))
  )
  private[this] lazy val controllers_Categorys_save15_invoker = createInvoker(
    Categorys_0.save,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Categorys",
      "save",
      Nil,
      "POST",
      """""",
      this.prefix + """api/categorys/save"""
    )
  )

  // @LINE:28
  private[this] lazy val controllers_Categorys_show16_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/categorys/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Categorys_show16_invoker = createInvoker(
    Categorys_0.show(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Categorys",
      "show",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """api/categorys/$id<[^/]+>"""
    )
  )

  // @LINE:29
  private[this] lazy val controllers_Categorys_update17_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/categorys/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Categorys_update17_invoker = createInvoker(
    Categorys_0.update(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Categorys",
      "update",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/categorys/$id<[^/]+>"""
    )
  )

  // @LINE:30
  private[this] lazy val controllers_Categorys_delete18_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/categorys/"), DynamicPart("id", """[^/]+""",true), StaticPart("/delete")))
  )
  private[this] lazy val controllers_Categorys_delete18_invoker = createInvoker(
    Categorys_0.delete(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Categorys",
      "delete",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/categorys/$id<[^/]+>/delete"""
    )
  )

  // @LINE:33
  private[this] lazy val controllers_Vouchers_list19_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/vouchers")))
  )
  private[this] lazy val controllers_Vouchers_list19_invoker = createInvoker(
    Vouchers_5.list(fakeValue[Int], fakeValue[Int], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Vouchers",
      "list",
      Seq(classOf[Int], classOf[Int], classOf[String]),
      "GET",
      """ Vouchers list (look at the default values for pagination parameters)""",
      this.prefix + """api/vouchers"""
    )
  )

  // @LINE:34
  private[this] lazy val controllers_Vouchers_listall20_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/vouchers/all")))
  )
  private[this] lazy val controllers_Vouchers_listall20_invoker = createInvoker(
    Vouchers_5.listall,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Vouchers",
      "listall",
      Nil,
      "GET",
      """""",
      this.prefix + """api/vouchers/all"""
    )
  )

  // @LINE:35
  private[this] lazy val controllers_Vouchers_save21_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/vouchers/save")))
  )
  private[this] lazy val controllers_Vouchers_save21_invoker = createInvoker(
    Vouchers_5.save,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Vouchers",
      "save",
      Nil,
      "POST",
      """""",
      this.prefix + """api/vouchers/save"""
    )
  )

  // @LINE:36
  private[this] lazy val controllers_Vouchers_show22_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/vouchers/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Vouchers_show22_invoker = createInvoker(
    Vouchers_5.show(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Vouchers",
      "show",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """api/vouchers/$id<[^/]+>"""
    )
  )

  // @LINE:37
  private[this] lazy val controllers_Vouchers_update23_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/vouchers/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Vouchers_update23_invoker = createInvoker(
    Vouchers_5.update(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Vouchers",
      "update",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/vouchers/$id<[^/]+>"""
    )
  )

  // @LINE:38
  private[this] lazy val controllers_Vouchers_delete24_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/vouchers/"), DynamicPart("id", """[^/]+""",true), StaticPart("/delete")))
  )
  private[this] lazy val controllers_Vouchers_delete24_invoker = createInvoker(
    Vouchers_5.delete(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Vouchers",
      "delete",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/vouchers/$id<[^/]+>/delete"""
    )
  )

  // @LINE:41
  private[this] lazy val controllers_Variants_list25_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/variants")))
  )
  private[this] lazy val controllers_Variants_list25_invoker = createInvoker(
    Variants_3.list(fakeValue[Int], fakeValue[Int], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Variants",
      "list",
      Seq(classOf[Int], classOf[Int], classOf[String]),
      "GET",
      """ Variants list (look at the default values for pagination parameters)""",
      this.prefix + """api/variants"""
    )
  )

  // @LINE:42
  private[this] lazy val controllers_Variants_listall26_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/variants/all")))
  )
  private[this] lazy val controllers_Variants_listall26_invoker = createInvoker(
    Variants_3.listall,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Variants",
      "listall",
      Nil,
      "GET",
      """""",
      this.prefix + """api/variants/all"""
    )
  )

  // @LINE:43
  private[this] lazy val controllers_Variants_save27_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/variants/save")))
  )
  private[this] lazy val controllers_Variants_save27_invoker = createInvoker(
    Variants_3.save,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Variants",
      "save",
      Nil,
      "POST",
      """""",
      this.prefix + """api/variants/save"""
    )
  )

  // @LINE:44
  private[this] lazy val controllers_Variants_show28_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/variants/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Variants_show28_invoker = createInvoker(
    Variants_3.show(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Variants",
      "show",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """api/variants/$id<[^/]+>"""
    )
  )

  // @LINE:45
  private[this] lazy val controllers_Variants_update29_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/variants/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Variants_update29_invoker = createInvoker(
    Variants_3.update(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Variants",
      "update",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/variants/$id<[^/]+>"""
    )
  )

  // @LINE:46
  private[this] lazy val controllers_Variants_delete30_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/variants/"), DynamicPart("id", """[^/]+""",true), StaticPart("/delete")))
  )
  private[this] lazy val controllers_Variants_delete30_invoker = createInvoker(
    Variants_3.delete(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Variants",
      "delete",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/variants/$id<[^/]+>/delete"""
    )
  )

  // @LINE:49
  private[this] lazy val controllers_Carts_list31_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/carts")))
  )
  private[this] lazy val controllers_Carts_list31_invoker = createInvoker(
    Carts_6.list,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Carts",
      "list",
      Nil,
      "GET",
      """ Carts list (look at the default values for pagination parameters)""",
      this.prefix + """api/carts"""
    )
  )

  // @LINE:50
  private[this] lazy val controllers_Carts_save32_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/carts/save")))
  )
  private[this] lazy val controllers_Carts_save32_invoker = createInvoker(
    Carts_6.save,
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Carts",
      "save",
      Nil,
      "POST",
      """""",
      this.prefix + """api/carts/save"""
    )
  )

  // @LINE:51
  private[this] lazy val controllers_Carts_show33_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/carts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Carts_show33_invoker = createInvoker(
    Carts_6.show(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Carts",
      "show",
      Seq(classOf[String]),
      "GET",
      """""",
      this.prefix + """api/carts/$id<[^/]+>"""
    )
  )

  // @LINE:52
  private[this] lazy val controllers_Carts_update34_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/carts/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Carts_update34_invoker = createInvoker(
    Carts_6.update(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Carts",
      "update",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/carts/$id<[^/]+>"""
    )
  )

  // @LINE:53
  private[this] lazy val controllers_Carts_delete35_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/carts/"), DynamicPart("id", """[^/]+""",true), StaticPart("/delete")))
  )
  private[this] lazy val controllers_Carts_delete35_invoker = createInvoker(
    Carts_6.delete(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Carts",
      "delete",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/carts/$id<[^/]+>/delete"""
    )
  )

  // @LINE:57
  private[this] lazy val controllers_Cartitems_list36_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cartitems/"), DynamicPart("cart", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Cartitems_list36_invoker = createInvoker(
    Cartitems_4.list(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Cartitems",
      "list",
      Seq(classOf[String]),
      "GET",
      """ Cartitems list (look at the default values for pagination parameters)""",
      this.prefix + """api/cartitems/$cart<[^/]+>"""
    )
  )

  // @LINE:58
  private[this] lazy val controllers_Cartitems_save37_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cartitems/"), DynamicPart("cart", """[^/]+""",true), StaticPart("/save")))
  )
  private[this] lazy val controllers_Cartitems_save37_invoker = createInvoker(
    Cartitems_4.save(fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Cartitems",
      "save",
      Seq(classOf[String]),
      "POST",
      """""",
      this.prefix + """api/cartitems/$cart<[^/]+>/save"""
    )
  )

  // @LINE:59
  private[this] lazy val controllers_Cartitems_show38_route = Route("GET",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cartitems/"), DynamicPart("cart", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Cartitems_show38_invoker = createInvoker(
    Cartitems_4.show(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Cartitems",
      "show",
      Seq(classOf[String], classOf[String]),
      "GET",
      """""",
      this.prefix + """api/cartitems/$cart<[^/]+>/$id<[^/]+>"""
    )
  )

  // @LINE:60
  private[this] lazy val controllers_Cartitems_update39_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cartitems/"), DynamicPart("cart", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true)))
  )
  private[this] lazy val controllers_Cartitems_update39_invoker = createInvoker(
    Cartitems_4.update(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Cartitems",
      "update",
      Seq(classOf[String], classOf[String]),
      "POST",
      """""",
      this.prefix + """api/cartitems/$cart<[^/]+>/$id<[^/]+>"""
    )
  )

  // @LINE:61
  private[this] lazy val controllers_Cartitems_delete40_route = Route("POST",
    PathPattern(List(StaticPart(this.prefix), StaticPart(this.defaultPrefix), StaticPart("api/cartitems/"), DynamicPart("cart", """[^/]+""",true), StaticPart("/"), DynamicPart("id", """[^/]+""",true), StaticPart("/delete")))
  )
  private[this] lazy val controllers_Cartitems_delete40_invoker = createInvoker(
    Cartitems_4.delete(fakeValue[String], fakeValue[String]),
    HandlerDef(this.getClass.getClassLoader,
      "router",
      "controllers.Cartitems",
      "delete",
      Seq(classOf[String], classOf[String]),
      "POST",
      """""",
      this.prefix + """api/cartitems/$cart<[^/]+>/$id<[^/]+>/delete"""
    )
  )


  def routes: PartialFunction[RequestHeader, Handler] = {
  
    // @LINE:6
    case controllers_Products_index0_route(params) =>
      call { 
        controllers_Products_index0_invoker.call(Products_2.index)
      }
  
    // @LINE:9
    case controllers_Products_list1_route(params) =>
      call(params.fromQuery[Int]("p", Some(0)), params.fromQuery[Int]("s", Some(2)), params.fromQuery[String]("f", Some(""))) { (p, s, f) =>
        controllers_Products_list1_invoker.call(Products_2.list(p, s, f))
      }
  
    // @LINE:10
    case controllers_Products_listall2_route(params) =>
      call { 
        controllers_Products_listall2_invoker.call(Products_2.listall)
      }
  
    // @LINE:11
    case controllers_Products_save3_route(params) =>
      call { 
        controllers_Products_save3_invoker.call(Products_2.save)
      }
  
    // @LINE:12
    case controllers_Products_show4_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Products_show4_invoker.call(Products_2.show(id))
      }
  
    // @LINE:13
    case controllers_Products_update5_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Products_update5_invoker.call(Products_2.update(id))
      }
  
    // @LINE:14
    case controllers_Products_delete6_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Products_delete6_invoker.call(Products_2.delete(id))
      }
  
    // @LINE:17
    case controllers_Brands_list7_route(params) =>
      call(params.fromQuery[Int]("p", Some(0)), params.fromQuery[Int]("s", Some(2)), params.fromQuery[String]("f", Some(""))) { (p, s, f) =>
        controllers_Brands_list7_invoker.call(Brands_1.list(p, s, f))
      }
  
    // @LINE:18
    case controllers_Brands_listall8_route(params) =>
      call { 
        controllers_Brands_listall8_invoker.call(Brands_1.listall)
      }
  
    // @LINE:19
    case controllers_Brands_save9_route(params) =>
      call { 
        controllers_Brands_save9_invoker.call(Brands_1.save)
      }
  
    // @LINE:20
    case controllers_Brands_show10_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Brands_show10_invoker.call(Brands_1.show(id))
      }
  
    // @LINE:21
    case controllers_Brands_update11_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Brands_update11_invoker.call(Brands_1.update(id))
      }
  
    // @LINE:22
    case controllers_Brands_delete12_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Brands_delete12_invoker.call(Brands_1.delete(id))
      }
  
    // @LINE:25
    case controllers_Categorys_list13_route(params) =>
      call(params.fromQuery[Int]("p", Some(0)), params.fromQuery[Int]("s", Some(2)), params.fromQuery[String]("f", Some(""))) { (p, s, f) =>
        controllers_Categorys_list13_invoker.call(Categorys_0.list(p, s, f))
      }
  
    // @LINE:26
    case controllers_Categorys_listall14_route(params) =>
      call { 
        controllers_Categorys_listall14_invoker.call(Categorys_0.listall)
      }
  
    // @LINE:27
    case controllers_Categorys_save15_route(params) =>
      call { 
        controllers_Categorys_save15_invoker.call(Categorys_0.save)
      }
  
    // @LINE:28
    case controllers_Categorys_show16_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Categorys_show16_invoker.call(Categorys_0.show(id))
      }
  
    // @LINE:29
    case controllers_Categorys_update17_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Categorys_update17_invoker.call(Categorys_0.update(id))
      }
  
    // @LINE:30
    case controllers_Categorys_delete18_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Categorys_delete18_invoker.call(Categorys_0.delete(id))
      }
  
    // @LINE:33
    case controllers_Vouchers_list19_route(params) =>
      call(params.fromQuery[Int]("p", Some(0)), params.fromQuery[Int]("s", Some(2)), params.fromQuery[String]("f", Some(""))) { (p, s, f) =>
        controllers_Vouchers_list19_invoker.call(Vouchers_5.list(p, s, f))
      }
  
    // @LINE:34
    case controllers_Vouchers_listall20_route(params) =>
      call { 
        controllers_Vouchers_listall20_invoker.call(Vouchers_5.listall)
      }
  
    // @LINE:35
    case controllers_Vouchers_save21_route(params) =>
      call { 
        controllers_Vouchers_save21_invoker.call(Vouchers_5.save)
      }
  
    // @LINE:36
    case controllers_Vouchers_show22_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Vouchers_show22_invoker.call(Vouchers_5.show(id))
      }
  
    // @LINE:37
    case controllers_Vouchers_update23_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Vouchers_update23_invoker.call(Vouchers_5.update(id))
      }
  
    // @LINE:38
    case controllers_Vouchers_delete24_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Vouchers_delete24_invoker.call(Vouchers_5.delete(id))
      }
  
    // @LINE:41
    case controllers_Variants_list25_route(params) =>
      call(params.fromQuery[Int]("p", Some(0)), params.fromQuery[Int]("s", Some(2)), params.fromQuery[String]("f", Some(""))) { (p, s, f) =>
        controllers_Variants_list25_invoker.call(Variants_3.list(p, s, f))
      }
  
    // @LINE:42
    case controllers_Variants_listall26_route(params) =>
      call { 
        controllers_Variants_listall26_invoker.call(Variants_3.listall)
      }
  
    // @LINE:43
    case controllers_Variants_save27_route(params) =>
      call { 
        controllers_Variants_save27_invoker.call(Variants_3.save)
      }
  
    // @LINE:44
    case controllers_Variants_show28_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Variants_show28_invoker.call(Variants_3.show(id))
      }
  
    // @LINE:45
    case controllers_Variants_update29_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Variants_update29_invoker.call(Variants_3.update(id))
      }
  
    // @LINE:46
    case controllers_Variants_delete30_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Variants_delete30_invoker.call(Variants_3.delete(id))
      }
  
    // @LINE:49
    case controllers_Carts_list31_route(params) =>
      call { 
        controllers_Carts_list31_invoker.call(Carts_6.list)
      }
  
    // @LINE:50
    case controllers_Carts_save32_route(params) =>
      call { 
        controllers_Carts_save32_invoker.call(Carts_6.save)
      }
  
    // @LINE:51
    case controllers_Carts_show33_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Carts_show33_invoker.call(Carts_6.show(id))
      }
  
    // @LINE:52
    case controllers_Carts_update34_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Carts_update34_invoker.call(Carts_6.update(id))
      }
  
    // @LINE:53
    case controllers_Carts_delete35_route(params) =>
      call(params.fromPath[String]("id", None)) { (id) =>
        controllers_Carts_delete35_invoker.call(Carts_6.delete(id))
      }
  
    // @LINE:57
    case controllers_Cartitems_list36_route(params) =>
      call(params.fromPath[String]("cart", None)) { (cart) =>
        controllers_Cartitems_list36_invoker.call(Cartitems_4.list(cart))
      }
  
    // @LINE:58
    case controllers_Cartitems_save37_route(params) =>
      call(params.fromPath[String]("cart", None)) { (cart) =>
        controllers_Cartitems_save37_invoker.call(Cartitems_4.save(cart))
      }
  
    // @LINE:59
    case controllers_Cartitems_show38_route(params) =>
      call(params.fromPath[String]("cart", None), params.fromPath[String]("id", None)) { (cart, id) =>
        controllers_Cartitems_show38_invoker.call(Cartitems_4.show(cart, id))
      }
  
    // @LINE:60
    case controllers_Cartitems_update39_route(params) =>
      call(params.fromPath[String]("cart", None), params.fromPath[String]("id", None)) { (cart, id) =>
        controllers_Cartitems_update39_invoker.call(Cartitems_4.update(cart, id))
      }
  
    // @LINE:61
    case controllers_Cartitems_delete40_route(params) =>
      call(params.fromPath[String]("cart", None), params.fromPath[String]("id", None)) { (cart, id) =>
        controllers_Cartitems_delete40_invoker.call(Cartitems_4.delete(cart, id))
      }
  }
}