
// @GENERATOR:play-routes-compiler
// @SOURCE:/Volumes/Data/hrm/conf/routes
// @DATE:Thu Dec 31 23:11:17 WIB 2015


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
